﻿using Microsoft.Win32;
using System;
using System.Windows.Forms;

namespace WMB.GeraChaveWindows
{
    public partial class Form1 : Form
    {
        private Guid newGuid = Guid.NewGuid();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label1.Text = getRegistry();
            textBox1.Text = newGuid.ToString();
            GravaNewGuid(newGuid);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var id = new Guid(textBox1.Text);
            GravaNewGuid(id);
            textBox1.Text = Guid.NewGuid().ToString();
            label1.Text = getRegistry();

        }

        public void GravaNewGuid(Guid newGuid)
        {
            using (var view32 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine,
                                            RegistryView.Registry64))
            {
                using (var clsid32 = view32.OpenSubKey("SOFTWARE\\Microsoft\\Cryptography", true))
                {
                    clsid32.SetValue("MachineGuid", newGuid.ToString(), Microsoft.Win32.RegistryValueKind.String);
                }
            }
        }

        public void deleteRegistry()
        {
            using (RegistryKey key = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Cryptography", true))
            {
                if (key != null)
                {
                    key.DeleteValue("MachineGuid");
                }
            }
        }
        public string getRegistry()
        {
            using (var view32 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine,
                                           RegistryView.Registry64))
            {
                using (var clsid32 = view32.OpenSubKey("SOFTWARE\\Microsoft\\Cryptography", true))
                {
                    return clsid32.GetValue("MachineGuid").ToString();
                }
            }




        }


    }
}
